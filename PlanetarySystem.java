import java.util.Random;
/**
 * class PlanetarySystem
 */
public class PlanetarySystem
{
    String systemName;
    Planet [] planetList;
    PlanetarySystem (String name, Planet [] planets)
    {
        this.systemName = name;
        this.planetList = planets;
    }
    Planet randomPlanet() 
    {
        Random rng = new Random();
        if (!(planetList == null || planetList.length == 0)) 
        {
            int index = rng.nextInt(planetList.length);
            return planetList[index];
        }
        return null;
       
    } 
}