
/**
 * SpaceAdventure Game for reviewing ICS3U1 Material for ICS4U1
 *
 * Space A group
 * Febuary 06, 2018
 */
import java.util.Scanner;
import java.io.InputStream;
public class SpaceAdventure
{
    PlanetarySystem currentSystem; //Initialize a PlanetarySystem called currentSystem (Note null value)
    private static Planet[] planetAmount = new Planet[8];
    
    public SpaceAdventure(){
        //Initialize Planets
        /*Planet mercury = new Planet("Mercury", "A very hot planet, closest to the sun.");
        Planet venus = new Planet("Venus", "A planet with very long days, second from the sun.");
        Planet mars = new Planet("Mars", "The red planet, fourth from the sun.");
        Planet jupiter = new Planet("Jupiter", "A very massive planet, fifth from the sun.");
        Planet saturn = new Planet("Saturn", "A gas giant with a ring system, sixth from the sun.");
        Planet uranus = new Planet("Uranus", "An ice giant, seventh from the sun.");
        Planet neptune = new Planet("Neptune", "A very cold planet, furthest from the sun.");
        planetAmount[0] = mercury;
        planetAmount[1] = venus;
        planetAmount[2] = mars;
        planetAmount[3] = jupiter;
        planetAmount[4] = saturn;
        planetAmount[5] = uranus;
        planetAmount[6] = neptune;*/
        //Old Planet Initializer
        currentSystem = new PlanetarySystem("Solar System", planetAmount); //Assign values to the currentSystem Planetary System
    }
    
    public Scanner load(String planetInfo){
        InputStream file = getClass().getResourceAsStream(planetInfo);
        if(file != null) {
            return new Scanner(file);
        }
        return null;
    }
    
    public static void Main(){
        SpaceAdventure adventure = new SpaceAdventure();
        Scanner planetScan = adventure.load("planetInfo.txt");
        for(int i = 0; i < adventure.planetAmount.length; i++){
            String name = planetScan.nextLine();
            String description = planetScan.nextLine();
            planetAmount[i] = new Planet(name, description);
        }
        adventure.start();
    }

    public void start(){
        displayIntroduction();
        greetAdventurer();
        if(!(currentSystem.planetList == null || currentSystem.planetList.length == 0)){
            determineDestination(); //error catching, make sure planets are initialized and assigned values
        }
    }

    private static String responseToPrompt(String prompt){
        //Used to ask a question and return the answer
        System.out.println(prompt);
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }

    private void displayIntroduction() {

        final double circumferenceOfEarth = 24859.82;
        System.out.println("Welcome to the " + currentSystem.systemName + "!");
        System.out.println("There are " + currentSystem.planetList.length + " planets to explore.");
        System.out.println("You are currently on Earth, which has a circumference of " + Double.toString(circumferenceOfEarth) + " miles.");
    }

    private void greetAdventurer(){
        String name = responseToPrompt("What is your name?");
        System.out.println("Nice to meet you, " + name + ". My name is Eliza.");
    }

    private void determineDestination(){
        String randomPlanet = "";
        while(!(randomPlanet.equalsIgnoreCase("Y") || randomPlanet.equalsIgnoreCase("N"))){
            randomPlanet = responseToPrompt("Let's go on an adventure! Shall I randomly choose a planet for you to visit? (Y or N)");
            if (randomPlanet.equalsIgnoreCase("Y")){
                Planet planetChoice = currentSystem.randomPlanet();
                if(planetChoice != null){
                    visit(planetChoice.planetName);
                }else{
                    System.out.println("Sorry, but there are no planets in this system.");
                }
            }else if (randomPlanet.equalsIgnoreCase("N")){
                String choice = responseToPrompt("Ok, name the planet you would like to visit.");
                visit(choice);
            }else{
                System.out.println("Huh? Sorry I didn't get that.");
            }
        }
    }

    private void visit(String planetName){
        System.out.println("Traveling to " + planetName + "...");
        boolean foundPlanet = false; //check if planet choice matches any planets that are a part of currentSystem
        for(Planet planet: currentSystem.planetList){
            if(planetName.equalsIgnoreCase(planet.planetName)){
                System.out.println("Arrived at " + planet.planetName + ". " + planet.planetDescription);
                foundPlanet = true;
                break;
            } else {
                foundPlanet = false;
            }
        }
        if(foundPlanet == false){
            System.out.println("Sorry, I could not found " + planetName + ".");
            String choice = responseToPrompt("Name the planet you would like to visit.");
            visit(choice);
        }
    }

}
